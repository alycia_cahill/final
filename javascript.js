// Form Validation

const validateUser = function(submitEvent){
    const user = document.getElementById('inputusername'); 
    if(user.value.length < 4){
        const userParent = user.parentElement; 
        userParent.classList.add("is-invalid"); 
        submitEvent.preventDefault(); 
    } 
}

const validateEmail = function(submitEvent){
    const email = document.getElementById("inputemail");  
    const emailFormat = /\w+@\w+\.\w+/; 
    if(!email.value.match(emailFormat)){
        const emailParent = email.parentElement; 
        emailParent.classList.add("is-invalid"); 
        const emailHelp = document.getElementById("emailHelp"); 
        emailHelp.innerHTML = "Need some Kelp?  You email needs to be the proper format."
        submitEvent.preventDefault();
    }
}

const validatePassword = function(submitEvent){
    const password = document.getElementById("inputpassword"); 
    const passwordFormat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if(!password.value.match(passwordFormat)){
        const passwordParent = password.parentElement; 
        passwordParent.classList.add("is-invalid"); 
        submitEvent.preventDefault(); 
    }
}

const validateZip = function(submitEvent){
    const zip = document.getElementById("inputZip"); 
    const zipFormat = /^[0-9]{5}$/; 
    if(!zip.value.match(zipFormat)){
        const zipParent = zip.parentElement; 
        zipParent.classList.add("is-invalid"); 
        submitEvent.preventDefault(); 
    }

}

const formSubmit = document.getElementById("join").addEventListener("submit", function(e){
    validateUser(e);
    validateEmail(e); 
    validatePassword(e); 
    validateZip(e); 
}); 



//For Article Feed at Bottom 

const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';

const url = `${BASE_URL}?q=gardening&api-key=AZPYcdu7JHCd1J8GP0pvwnK5AFxb998s`;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    //console.log(responseJson);

    let articles = responseJson.response.docs; // is the array of articles
    let articleContainer = document.getElementById('article-feed'); //gets the container they will go in

    for (i=0; i<3; i++){
        let headline = articles[i].headline.main; 
        let snippet = articles[i].snippet; 
        let link = articles[i].web_url; 
        
        let headlineContainer = document.createElement("H5"); 
        let displayHeadline = document.createTextNode(headline); 
        headlineContainer.appendChild(displayHeadline); 

        let linkA = document.createElement("A"); 
        linkA.innerHTML = "Read More"; 
        linkA.setAttribute("href", link); 
        
        let snippetContainer = document.createElement("P"); 
        let displaySnippet = document.createTextNode(snippet); 
        snippetContainer.appendChild(displaySnippet); 

        
        let articleFeed = document.getElementById("article-feed");
        articleFeed.append(headlineContainer, snippetContainer, linkA); 
        
    }

  });

  //timing function set at 50000 for final
  setTimeout(function(){ alert("Are you still there?"); }, 5000);

  //Constructor function witj Prototype 

  function veggieSwap(userName, quantity, veggie){
    this.userName = userName; 
    this.quantity = quantity; 
    this.veggie = veggie; 
  }

  veggieSwap.prototype.displayVeggieSwap = function(){
      let li = document.createElement("LI"); 
      let liText =  "User Name: " + this.userName + " - Amount: " + this.quantity + " - Has to Trade: "  + this.veggie; 
      let liContent = document.createTextNode(liText); 
      li.append(liContent); 
      return li; 
  }

  var userOne = new veggieSwap("KindOfABigDill", 50, "Cucumber"); 
  var userTwo = new veggieSwap("PeterPiper", 100, "Peppers");
  var userThree = new veggieSwap("EverydayImBrusselin", 54, "Brussel Sprouts"); 

  var list = document.getElementById("veggie-list"); 

  list.append(userOne.displayVeggieSwap()); 
  list.append(userTwo.displayVeggieSwap()); 
  list.append(userThree.displayVeggieSwap()); 






  
